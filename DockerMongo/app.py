import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow
import acp_times
import config
import logging

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

db.tododb.delete_many({})

@app.route('/')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html',items= items)
    return render_template('calc.html')

@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brev_dist = request.args.get('brev_dis', 999, type = int)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brev_dist, arrow.now().isoformat())
    close_time = acp_times.close_time(km, brev_dist, arrow.now().isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    # contents of text box are kept track of in items (saved)
    # and then displayed below
    if(len(items) ==0) :
        return render_template('empty.html')
    return render_template('display.html',items=items) # updates displayed items


    # todo.html has two blank text boxes and a submit button
    # entering text in the left box and hitting submit will
    # display it below in large bold while data entered in
    # right box displays it below in smaller unbolded letters
    # left box contents are displayed before right box contents
@app.route('/empty')
def empty():
	return render_template('empty.html')

@app.route('/new', methods=['POST'])
def new():
    # get the values the user entered/program displayed
    open_data = request.form.getlist("open")
    close_data= request.form.getlist("close")
    km = request.form.getlist("km")

    # our lists to store opentime(siftedO) closetime(siftedC)
    # and km(siftedK)
    Opened = []
    Closed = []
    kms = []

    # if the value isn't empty, then add it into the list
    for item in open_data:
        if (str(item) != ""): Opened.append(str(item))

    for item in close_data:
        if (str(item) != ""): Closed.append(str(item))

    for item in km:
        if (str(item) != ""): kms.append(str(item))

    # take the longest length list
    # one got bigger than the other. This length will iterate and print out for the longest list so as
    # to make sure nothing gets left behind
    length = max(len(Opened), len(Closed), len(kms))

    # if theres nothing in any list, and the user tries to
    # hit submit, then we'll give them an error
    if length == 0:
        return redirect(url_for('empty'))

    # go through the lists assign item values. These will be
    # printed out in display HTML later on. Previously from the
    # start app.py def new() method
    for i in range(length):
        item_doc = {
            'openDataDone': Opened[i],
            'closeDataDone': Closed[i],
            'kmDataDone': kms[i]
        }
        db.tododb.insert_one(item_doc)

    # once we are done with the set of values the user submitted, head back
    # to index (and thus main calc page) and do it again at home page
    return redirect(url_for('index'))







app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
