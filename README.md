# Project 5: Brevet time calculator with Ajax and MongoDB
## Author : Tae Ho Yun
## Email : jawookr1995@gmail.com

# Description
## Adding display and submit keys to proj4-brevet which calculate
## open and close time automatically based on ACP Controle times rules
## The km that we input should not exceed the brevet distance.
## Each time we submit the info they would be stored in db and display
## button will show the list of info that clients submitted.
## km value should be larger than 0.
## To run ACP calculator in mongodb envrionment, goto dockerMongo repo
## and docker-cmpose up and finally visit localhost.